import java.util.ArrayList;

public class Patient extends Person {
    private String patientId;
    private ArrayList<Illness> illnessList;

    public Patient(String name,
                   int age, 
                   String socialSecurityNumber,
                   String patientId,
                   ArrayList<Illness> illnessList){
        super(name, age, socialSecurityNumber);
        this.patientId = patientId;
        this.illnessList = illnessList;        
    }

    public String addIllness(Illness illness){
      String response = "";
      if (!illnessList.contains(illness)) {
        illnessList.add(illness);
        response = illness.getInfo() + " is added";
      } else {
        response = illness.getInfo() + " is already on the list";
      }

      return response;
    }
    

    public String getInfo(){
      String info = getName() + " " + getAge() + " "+ getSocialSecurityNumber()+ " "+ patientId + " - ";
      if (illnessList.size()>-1) {
        for (Illness illness : illnessList) {
            info = info + illness.getInfo();
        }  
      } else {
        info = info + " not illness";
      }
      return info;      
    }
    
    
}
