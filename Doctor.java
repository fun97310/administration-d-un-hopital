public class Doctor extends MedicalStaff {
    private String speciality;

    public Doctor(String name, int age, String socialSecurityNumber, String employeeId, String speciality){
        super(name, age, socialSecurityNumber, employeeId);
        this.speciality = speciality;
    }

    public String getSpeciality(){
        return speciality;
    }

    public void getRole(){
        System.out.print("Doctor ");
    }

    public void careForPatient(Patient patient){
        this.getRole();
        System.out.println(this.getName() + " cares for "+ patient.getName() );
    }
}
