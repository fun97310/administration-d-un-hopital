public abstract class Person {
    private String name;
    private int age;
    private String socialSecurityNumber;

    public Person(String name, int age, String socialSecurityNumber){
        this.name = name;
        this.age = age;
        this.socialSecurityNumber = socialSecurityNumber;
    }

    public String getName(){
        return name;
    }

    public int getAge(){
        return age;
    }

    public String getSocialSecurityNumber(){
        return socialSecurityNumber;
    }

    public String getInfoPerson(){
        return name + " "+ age + " "+ socialSecurityNumber+ " ";
    }
}
