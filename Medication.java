public class Medication {
    private String name;
    private String dosage;

    public Medication(String name, String dosage){
        this.name = name;
        this.dosage = dosage;
    }

    public String getName(){
        return name;
    }

    public String getInfo(){
        String info = name + " "+ dosage;
        return info;
    }
}
