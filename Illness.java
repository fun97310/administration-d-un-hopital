import java.util.ArrayList;

public class Illness {
    private String name;
    private ArrayList<Medication> medicationList;

    public Illness(String name,
                   ArrayList<Medication> medicationList){
        this.name = name;
        this.medicationList = medicationList;
    }
    // addMedication 
    public String addMedication(Medication medication){
        String response = "";
        if (!medicationList.contains(medication)) {
            medicationList.add(medication);
            response = medication.getName() + " is added";
        } else {
            response = medication.getName() + " is already on the list";
        }

        return response;
    }

    public String getInfo(){
        String info = name + " (";
        for (Medication medication : medicationList) {
            info += " " + medication.getInfo() + " ";
        }
        info += " )";
        return info;
    }
}
