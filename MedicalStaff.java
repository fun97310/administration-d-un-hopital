abstract public class MedicalStaff extends Person implements Care {
    //Attributs : employeeId (String) Méthodes abstraites : getRole()
    private String employeeId;

    public MedicalStaff(String name, int age, String socialSecurityNumber, String employeeId){
        super(name, age, socialSecurityNumber);
        this.employeeId = employeeId;
    }
    public String getId(){
        return employeeId;
    }

    abstract public void getRole();

}
