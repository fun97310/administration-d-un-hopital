public class Nurse extends MedicalStaff {
    public Nurse(String name, int age, String socialSecurityNumber, String employeeId){
        super(name, age, socialSecurityNumber, employeeId);
    }
    public void getRole(){
        System.out.print("Nurse ");
    }
    public void careForPatient(Patient patient){
        this.getRole();
        System.out.println(this.getName() + " cares for "+ patient.getName() );
    }
}
