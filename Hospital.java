import java.util.ArrayList;
import java.util.Arrays;

public class Hospital{

    public static void main(String[] args) {
       
        Medication medication1 = new Medication("Paracetamol", "500mg");
        Medication medication2 = new Medication("Aspirine", "200mg");
        Medication medication3 = new Medication("Tramadol", "50mg");
        Medication medication4 = new Medication("Immodium", "180mg");
       // System.out.println(medication3.getInfo());
       // System.out.println(medication4.getInfo());

        Illness illness1 = new Illness("Constipation", new ArrayList<>(Arrays.asList(medication1, medication4)));
        Illness illness2 = new Illness("Fièvre jaune", new ArrayList<>(Arrays.asList(medication3, medication2)));

        System.out.println(illness1.getInfo());
        System.out.println(illness2.getInfo());


        Patient patient1 = new Patient("John Doe", 30, "123456789", "idpatient", new ArrayList<>(Arrays.asList(illness1)));
        Patient patient2 = new Patient("Jane Doe", 36, "168456789", "idpatient", new ArrayList<>(Arrays.asList( illness2)));

        
        System.out.println(illness1.addMedication(medication3));
        System.out.println(illness2.addMedication(medication1));
        System.out.println(illness2.addMedication(medication1));

        System.out.println(patient1.addIllness(illness1));
        System.out.println(patient2.getInfo());
        System.out.println(patient1);
        
        
        Doctor doctor1 = new Doctor("Smith", 40, "987654321","empId85476", "Cardiologist");
        Doctor doctor2 = new Doctor("Linkins", 21, "457654321","ampId86976", "Pneumologue");
        doctor1.careForPatient(patient1);
        doctor2.careForPatient(patient2);
        
        Nurse nurse1 = new Nurse("Johnson", 35, "654321987", "empid87546");
        nurse1.careForPatient(patient2);
    }
}